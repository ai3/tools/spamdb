spamdb
===

*spamdb* is a solution to manage user feedback for a SpamAssassin
Bayes database in a distributed environment.

The expected scenario sees a number of SpamAssassin instances on
different servers, and a single centralized *spamdb* service.  The
spamdb service maintains the authoritative copy of the Bayes database,
receives feedback submissions, and periodically pushes an updated
snapshot of the database to the servers running SpamAssassin.

Feedback submission is usually integrated into the IMAP server
(e.g. [Dovecot](https://wiki2.dovecot.org/HowTo/AntispamWithSieve)). As
it normally runs inline to the IMAP COPY command, latency is important
here: the *spamdb* submission mechanism sees the feedback tool send
requests to a local relay daemon running on the host via UNIX socket;
the relay server than takes care of forwarding the request to the
spamdb submission service over the network.

The design tolerates unavailability by discarding requests, accepting
that each invidivual feedback request only contributes a small amount
of data to the database.


## Building

The main *spamdb* program can be built with this command:

```shell
$ go build -o spamdb .
```

While the *spamdb-feedback* tool must be compiled separately from its
C source:

```shell
$ gcc -o spamdb-feedback -O2 -s c/submit.c
```


## Running

The *spamdb* tool supports a few commands to run the feedback server,
the relay server, and the client. Server commands require a
configuration file (in YAML) to specify network transport parameters
and URLs.

### `spamdb server`

Runs the server. Specify the paths 
