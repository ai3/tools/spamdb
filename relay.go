package main

import (
	"bufio"
	"context"
	"flag"
	"io/ioutil"
	"log"
	"net"
	"os"
	"os/signal"
	"syscall"
	"time"

	"git.autistici.org/ai3/go-common/clientutil"
	"git.autistici.org/ai3/go-common/unix"
	"github.com/coreos/go-systemd/v22/daemon"
	"github.com/google/subcommands"
)

var (
	relayQueueSize      = 1000
	relayRequestTimeout = 300 * time.Second
)

type relayConfig struct {
	Upstream *clientutil.BackendConfig `yaml:"upstream"`
}

type relayCommand struct {
	config relayConfig

	configPath              string
	socketPath              string
	systemdSocketActivation bool
}

func (c *relayCommand) Name() string     { return "relay" }
func (c *relayCommand) Synopsis() string { return "run the local relay" }
func (c *relayCommand) Usage() string {
	return `relay [<flags>]:
        Run the local relay.

`
}

func (c *relayCommand) SetFlags(f *flag.FlagSet) {
	f.StringVar(&c.configPath, "config", "/etc/spamdb/relay.yml", "configuration file `path`")
	f.StringVar(&c.socketPath, "socket", "/run/spamdb/socket", "socket `path` to listen on")
	f.BoolVar(&c.systemdSocketActivation, "systemd-socket", false, "use SystemD socket activation")
}

func (c *relayCommand) Execute(ctx context.Context, f *flag.FlagSet, args ...interface{}) subcommands.ExitStatus {
	if f.NArg() > 0 {
		log.Printf("error: too many arguments")
		return subcommands.ExitUsageError
	}

	if err := loadYAML(c.configPath, &c.config); err != nil {
		log.Printf("error loading configuration: %v", err)
		return subcommands.ExitUsageError
	}

	be, err := clientutil.NewBackend(c.config.Upstream)
	if err != nil {
		log.Printf("error creating connection to server: %v", err)
		return subcommands.ExitFailure
	}

	srv := newRelayServer(be)

	var sockSrv *unix.SocketServer
	if c.systemdSocketActivation {
		sockSrv, err = unix.NewSystemdSocketServer(srv)
	} else {
		sockSrv, err = unix.NewUNIXSocketServer(c.socketPath, srv)
	}
	if err != nil {
		log.Fatalf("error: %v", err)
	}

	done := make(chan struct{})
	sigCh := make(chan os.Signal, 1)
	go func() {
		<-sigCh
		log.Printf("terminating")
		sockSrv.Close()
		close(done)
	}()
	signal.Notify(sigCh, syscall.SIGINT, syscall.SIGTERM)

	log.Printf("starting")
	daemon.SdNotify(false, "READY=1") // nolint: errcheck
	if err := sockSrv.Serve(); err != nil {
		log.Fatal(err)
	}

	<-done

	return subcommands.ExitSuccess
}

type relayRequest struct {
	Type string
	Data string
}

type relayServer struct {
	ch       chan *relayRequest
	upstream clientutil.Backend
}

func newRelayServer(upstream clientutil.Backend) *relayServer {
	r := &relayServer{
		ch:       make(chan *relayRequest, relayQueueSize),
		upstream: upstream,
	}
	go r.worker()
	return r
}

func (r *relayServer) ServeConnection(c net.Conn) {
	defer c.Close()

	cr := bufio.NewReader(c)
	typ, err := cr.ReadString('\n')
	if err != nil {
		log.Printf("error reading request type: %v", err)
		return
	}

	data, err := ioutil.ReadAll(c)
	if err != nil {
		log.Printf("error reading data: %v", err)
		return
	}

	// To minimize latency on the IMAP client, stash the request
	// on the queue, or drop it right away if it is full.
	req := &relayRequest{
		Type: typ,
		Data: string(data),
	}
	select {
	case r.ch <- req:
		log.Printf("received request (%s)", typ)
	default:
		log.Printf("queue is overloaded, dropping submission")
	}
}

func (r *relayServer) worker() {
	for req := range r.ch {
		if err := r.submit(req); err != nil {
			log.Printf("submission error: %v", err)
		} else {
			log.Printf("forwarded request (%s)", req.Type)
		}
	}
}

func (r *relayServer) submit(req *relayRequest) error {
	ctx, cancel := context.WithTimeout(context.Background(), relayRequestTimeout)
	defer cancel()
	return r.upstream.Call(ctx, "/api/submit", "", &SubmissionRequest{
		Type: req.Type,
		Body: req.Data,
	}, nil)
}

func init() {
	subcommands.Register(&relayCommand{}, "")
}
