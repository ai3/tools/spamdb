package main

import (
	"context"
	"flag"
	"io/ioutil"
	"log"
	"os"

	"github.com/google/subcommands"
	"gopkg.in/yaml.v3"
)

func loadYAML(path string, obj interface{}) error {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}
	return yaml.Unmarshal(data, obj)
}

func init() {
	subcommands.Register(subcommands.HelpCommand(), "help")
	subcommands.Register(subcommands.CommandsCommand(), "help")
	subcommands.Register(subcommands.FlagsCommand(), "help")
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	os.Exit(int(subcommands.Execute(context.Background())))
}
