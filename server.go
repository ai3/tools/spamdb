package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"time"

	"git.autistici.org/ai3/go-common/serverutil"
	"github.com/google/subcommands"
)

type serverConfig struct {
	ServerConfig          *serverutil.ServerConfig `yaml:"http_server"`
	ExportDir             string                   `yaml:"export_dir"`
	DBDir                 string                   `yaml:"db_dir"`
	ExportIntervalSeconds time.Duration            `yaml:"export_interval_seconds"`
}

type serverCommand struct {
	config serverConfig

	configPath string
	dbPath     string
	addr       string
	numWorkers int
}

const (
	submissionHam  = "ham"
	submissionSpam = "spam"
)

type SubmissionRequest struct {
	Type string `json:"type"`
	Body string `json:"body"`
}

func (c *serverCommand) receiveSubmission(ctx context.Context, req *SubmissionRequest) error {
	if req.Type != submissionHam && req.Type != submissionSpam {
		return errors.New("invalid submission type")
	}

	cmd := exec.CommandContext(ctx, "sa-learn", "--dbpath", c.dbPath, "-u", "__GLOBAL__", "--no-sync", "--"+req.Type)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Stdin = strings.NewReader(req.Body)
	return cmd.Run()
}

var subCh chan *SubmissionRequest

func (c *serverCommand) worker() {
	for req := range subCh {
		if err := c.receiveSubmission(context.Background(), req); err != nil {
			log.Printf("sa-learn error: %v", err)
		} else {
			log.Printf("received submission (%s)", req.Type)
		}
	}
}

func handleSubmission(w http.ResponseWriter, httpReq *http.Request) {
	var req SubmissionRequest
	if !serverutil.DecodeJSONRequest(w, httpReq, &req) {
		return
	}

	// To minimize latency, handle the request in the background.
	subCh <- &req

	// Return an empty response.
	serverutil.EncodeJSONResponse(w, struct{}{})
}

func (c *serverCommand) dumpState() error {
	dest := filepath.Join(c.config.ExportDir, "bayes.lz4")
	tmpName := dest + ".tmp"
	defer func() {
		os.Remove(tmpName)
	}()

	cmd := exec.Command(
		"/bin/sh", "-c",
		fmt.Sprintf(
			"sa-learn --dbpath '%s' --backup | lz4c > '%s'",
			c.dbPath,
			tmpName,
		),
	)
	cmd.Stderr = os.Stderr
	cmd.Stdout = os.Stdout
	if err := cmd.Run(); err != nil {
		return err
	}

	return os.Rename(tmpName, dest)
}

func (c *serverCommand) cronProc() {
	t := time.NewTicker(time.Duration(c.config.ExportIntervalSeconds) * time.Second)
	defer t.Stop()
	for range t.C {
		if err := c.dumpState(); err != nil {
			log.Printf("state export failed: %v", err)
		}
	}
}

func (c *serverCommand) Name() string     { return "server" }
func (c *serverCommand) Synopsis() string { return "run the HTTP server" }
func (c *serverCommand) Usage() string {
	return `server [<flags>]:
        Run the HTTP server.

`
}

func (c *serverCommand) SetFlags(f *flag.FlagSet) {
	f.StringVar(&c.configPath, "config", "/etc/spamdb/server.yml", "configuration file `path`")
	f.StringVar(&c.addr, "addr", ":3109", "`address` to listen on")
	f.IntVar(&c.numWorkers, "workers", 2, "number of concurrent sa-learn workers")
}

func (c *serverCommand) Execute(ctx context.Context, f *flag.FlagSet, args ...interface{}) subcommands.ExitStatus {
	if f.NArg() > 0 {
		log.Printf("error: too many arguments")
		return subcommands.ExitUsageError
	}

	// Set configuration defaults.
	c.config.ExportDir = "/var/lib/spamdb/export"
	c.config.DBDir = "/var/lib/spamdb/db"
	c.config.ExportIntervalSeconds = 21600
	if err := loadYAML(c.configPath, &c.config); err != nil {
		log.Printf("error loading configuration: %v", err)
		return subcommands.ExitUsageError
	}
	c.dbPath = filepath.Join(c.config.DBDir, "bayes.db")

	subCh = make(chan *SubmissionRequest, 1000)
	for i := 0; i < c.numWorkers; i++ {
		go c.worker()
	}

	go c.cronProc()

	h := http.NewServeMux()
	h.HandleFunc("/api/submit", handleSubmission)
	h.Handle("/export/", http.StripPrefix("/export/", http.FileServer(http.Dir(c.config.ExportDir))))
	if err := serverutil.Serve(h, c.config.ServerConfig, c.addr); err != nil {
		log.Printf("error: %v", err)
		return subcommands.ExitFailure
	}

	return subcommands.ExitSuccess
}

func init() {
	subcommands.Register(&serverCommand{}, "")
}
