module git.autistici.org/ai3/tools/spamdb

go 1.14

require (
	git.autistici.org/ai3/go-common v0.0.0-20210118064555-73f00db54723
	github.com/coreos/go-systemd/v22 v22.1.0
	github.com/google/subcommands v1.2.0
	gopkg.in/yaml.v3 v3.0.0-20200506231410-2ff61e1afc86
)
